package org.jhipster.blog.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import org.jhipster.blog.domain.enumeration.TRANSTYPE;

/**
 * A ACMETransaction.
 */
@Entity
@Table(name = "acme_transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ACMETransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @NotNull
    @Column(name = "transaction_date", nullable = false)
    private LocalDate transactionDate;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @NotNull
    @Column(name = "transaction_remark", nullable = false)
    private String transactionRemark;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private TRANSTYPE transactionType;

    @NotNull
    @Column(name = "available_balance", nullable = false)
    private Double availableBalance;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public ACMETransaction accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public ACMETransaction transactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Double getAmount() {
        return amount;
    }

    public ACMETransaction amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransactionRemark() {
        return transactionRemark;
    }

    public ACMETransaction transactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
        return this;
    }

    public void setTransactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
    }

    public TRANSTYPE getTransactionType() {
        return transactionType;
    }

    public ACMETransaction transactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public void setTransactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public ACMETransaction availableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
        return this;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ACMETransaction aCMETransaction = (ACMETransaction) o;
        if (aCMETransaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aCMETransaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ACMETransaction{" +
            "id=" + getId() +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", amount=" + getAmount() +
            ", transactionRemark='" + getTransactionRemark() + "'" +
            ", transactionType='" + getTransactionType() + "'" +
            ", availableBalance=" + getAvailableBalance() +
            "}";
    }
}
