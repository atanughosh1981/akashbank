package org.jhipster.blog.domain.enumeration;

/**
 * The TRANSTYPE enumeration.
 */
public enum TRANSTYPE {
    DEBIT, CREDIT
}
