package org.jhipster.blog.repository;

import org.jhipster.blog.domain.ACMEBankAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ACMEBankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ACMEBankAccountRepository extends JpaRepository<ACMEBankAccount, Long> {

}
