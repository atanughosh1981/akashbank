package org.jhipster.blog.repository;

import org.jhipster.blog.domain.ACMETransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ACMETransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ACMETransactionRepository extends JpaRepository<ACMETransaction, Long> {

}
