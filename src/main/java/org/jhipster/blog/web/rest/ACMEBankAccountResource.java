package org.jhipster.blog.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.jhipster.blog.domain.ACMEBankAccount;
import org.jhipster.blog.repository.ACMEBankAccountRepository;
import org.jhipster.blog.web.rest.errors.BadRequestAlertException;
import org.jhipster.blog.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ACMEBankAccount.
 */
@RestController
@RequestMapping("/api")
public class ACMEBankAccountResource {

    private final Logger log = LoggerFactory.getLogger(ACMEBankAccountResource.class);

    private static final String ENTITY_NAME = "aCMEBankAccount";

    private final ACMEBankAccountRepository aCMEBankAccountRepository;

    public ACMEBankAccountResource(ACMEBankAccountRepository aCMEBankAccountRepository) {
        this.aCMEBankAccountRepository = aCMEBankAccountRepository;
    }

    /**
     * POST  /acme-bank-accounts : Create a new aCMEBankAccount.
     *
     * @param aCMEBankAccount the aCMEBankAccount to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aCMEBankAccount, or with status 400 (Bad Request) if the aCMEBankAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/acme-bank-accounts")
    @Timed
    public ResponseEntity<ACMEBankAccount> createACMEBankAccount(@Valid @RequestBody ACMEBankAccount aCMEBankAccount) throws URISyntaxException {
        log.debug("REST request to save ACMEBankAccount : {}", aCMEBankAccount);
        if (aCMEBankAccount.getId() != null) {
            throw new BadRequestAlertException("A new aCMEBankAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ACMEBankAccount result = aCMEBankAccountRepository.save(aCMEBankAccount);
        return ResponseEntity.created(new URI("/api/acme-bank-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /acme-bank-accounts : Updates an existing aCMEBankAccount.
     *
     * @param aCMEBankAccount the aCMEBankAccount to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aCMEBankAccount,
     * or with status 400 (Bad Request) if the aCMEBankAccount is not valid,
     * or with status 500 (Internal Server Error) if the aCMEBankAccount couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/acme-bank-accounts")
    @Timed
    public ResponseEntity<ACMEBankAccount> updateACMEBankAccount(@Valid @RequestBody ACMEBankAccount aCMEBankAccount) throws URISyntaxException {
        log.debug("REST request to update ACMEBankAccount : {}", aCMEBankAccount);
        if (aCMEBankAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ACMEBankAccount result = aCMEBankAccountRepository.save(aCMEBankAccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aCMEBankAccount.getId().toString()))
            .body(result);
    }

    /**
     * GET  /acme-bank-accounts : get all the aCMEBankAccounts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aCMEBankAccounts in body
     */
    @GetMapping("/acme-bank-accounts")
    @Timed
    public List<ACMEBankAccount> getAllACMEBankAccounts() {
        log.debug("REST request to get all ACMEBankAccounts");
        return aCMEBankAccountRepository.findAll();
    }

    /**
     * GET  /acme-bank-accounts/:id : get the "id" aCMEBankAccount.
     *
     * @param id the id of the aCMEBankAccount to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aCMEBankAccount, or with status 404 (Not Found)
     */
    @GetMapping("/acme-bank-accounts/{id}")
    @Timed
    public ResponseEntity<ACMEBankAccount> getACMEBankAccount(@PathVariable Long id) {
        log.debug("REST request to get ACMEBankAccount : {}", id);
        Optional<ACMEBankAccount> aCMEBankAccount = aCMEBankAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(aCMEBankAccount);
    }

    /**
     * DELETE  /acme-bank-accounts/:id : delete the "id" aCMEBankAccount.
     *
     * @param id the id of the aCMEBankAccount to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/acme-bank-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteACMEBankAccount(@PathVariable Long id) {
        log.debug("REST request to delete ACMEBankAccount : {}", id);

        aCMEBankAccountRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
