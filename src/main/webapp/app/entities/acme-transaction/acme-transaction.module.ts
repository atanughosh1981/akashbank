import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from 'app/shared';
import {
    ACMETransactionComponent,
    ACMETransactionDetailComponent,
    ACMETransactionUpdateComponent,
    ACMETransactionDeletePopupComponent,
    ACMETransactionDeleteDialogComponent,
    aCMETransactionRoute,
    aCMETransactionPopupRoute
} from './';

const ENTITY_STATES = [...aCMETransactionRoute, ...aCMETransactionPopupRoute];

@NgModule({
    imports: [TestSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ACMETransactionComponent,
        ACMETransactionDetailComponent,
        ACMETransactionUpdateComponent,
        ACMETransactionDeleteDialogComponent,
        ACMETransactionDeletePopupComponent
    ],
    entryComponents: [
        ACMETransactionComponent,
        ACMETransactionUpdateComponent,
        ACMETransactionDeleteDialogComponent,
        ACMETransactionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestACMETransactionModule {}
