import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IACMETransaction } from 'app/shared/model/acme-transaction.model';

type EntityResponseType = HttpResponse<IACMETransaction>;
type EntityArrayResponseType = HttpResponse<IACMETransaction[]>;

@Injectable({ providedIn: 'root' })
export class ACMETransactionService {
    private resourceUrl = SERVER_API_URL + 'api/acme-transactions';

    constructor(private http: HttpClient) {}

    create(aCMETransaction: IACMETransaction): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(aCMETransaction);
        return this.http
            .post<IACMETransaction>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(aCMETransaction: IACMETransaction): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(aCMETransaction);
        return this.http
            .put<IACMETransaction>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IACMETransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IACMETransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(aCMETransaction: IACMETransaction): IACMETransaction {
        const copy: IACMETransaction = Object.assign({}, aCMETransaction, {
            transactionDate:
                aCMETransaction.transactionDate != null && aCMETransaction.transactionDate.isValid()
                    ? aCMETransaction.transactionDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.transactionDate = res.body.transactionDate != null ? moment(res.body.transactionDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((aCMETransaction: IACMETransaction) => {
            aCMETransaction.transactionDate = aCMETransaction.transactionDate != null ? moment(aCMETransaction.transactionDate) : null;
        });
        return res;
    }
}
