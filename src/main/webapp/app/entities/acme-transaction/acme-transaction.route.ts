import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ACMETransaction } from 'app/shared/model/acme-transaction.model';
import { ACMETransactionService } from './acme-transaction.service';
import { ACMETransactionComponent } from './acme-transaction.component';
import { ACMETransactionDetailComponent } from './acme-transaction-detail.component';
import { ACMETransactionUpdateComponent } from './acme-transaction-update.component';
import { ACMETransactionDeletePopupComponent } from './acme-transaction-delete-dialog.component';
import { IACMETransaction } from 'app/shared/model/acme-transaction.model';

@Injectable({ providedIn: 'root' })
export class ACMETransactionResolve implements Resolve<IACMETransaction> {
    constructor(private service: ACMETransactionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((aCMETransaction: HttpResponse<ACMETransaction>) => aCMETransaction.body));
        }
        return of(new ACMETransaction());
    }
}

export const aCMETransactionRoute: Routes = [
    {
        path: 'acme-transaction',
        component: ACMETransactionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMETransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-transaction/:id/view',
        component: ACMETransactionDetailComponent,
        resolve: {
            aCMETransaction: ACMETransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMETransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-transaction/new',
        component: ACMETransactionUpdateComponent,
        resolve: {
            aCMETransaction: ACMETransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMETransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-transaction/:id/edit',
        component: ACMETransactionUpdateComponent,
        resolve: {
            aCMETransaction: ACMETransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMETransactions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const aCMETransactionPopupRoute: Routes = [
    {
        path: 'acme-transaction/:id/delete',
        component: ACMETransactionDeletePopupComponent,
        resolve: {
            aCMETransaction: ACMETransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMETransactions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
