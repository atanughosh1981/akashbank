import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IACMETransaction } from 'app/shared/model/acme-transaction.model';
import { ACMETransactionService } from './acme-transaction.service';

@Component({
    selector: 'jhi-acme-transaction-update',
    templateUrl: './acme-transaction-update.component.html'
})
export class ACMETransactionUpdateComponent implements OnInit {
    private _aCMETransaction: IACMETransaction;
    isSaving: boolean;
    transactionDateDp: any;

    constructor(private aCMETransactionService: ACMETransactionService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ aCMETransaction }) => {
            this.aCMETransaction = aCMETransaction;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.aCMETransaction.id !== undefined) {
            this.subscribeToSaveResponse(this.aCMETransactionService.update(this.aCMETransaction));
        } else {
            this.subscribeToSaveResponse(this.aCMETransactionService.create(this.aCMETransaction));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IACMETransaction>>) {
        result.subscribe((res: HttpResponse<IACMETransaction>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get aCMETransaction() {
        return this._aCMETransaction;
    }

    set aCMETransaction(aCMETransaction: IACMETransaction) {
        this._aCMETransaction = aCMETransaction;
    }
}
