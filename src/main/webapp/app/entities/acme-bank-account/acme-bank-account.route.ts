import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ACMEBankAccount } from 'app/shared/model/acme-bank-account.model';
import { ACMEBankAccountService } from './acme-bank-account.service';
import { ACMEBankAccountComponent } from './acme-bank-account.component';
import { ACMEBankAccountDetailComponent } from './acme-bank-account-detail.component';
import { ACMEBankAccountUpdateComponent } from './acme-bank-account-update.component';
import { ACMEBankAccountDeletePopupComponent } from './acme-bank-account-delete-dialog.component';
import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

@Injectable({ providedIn: 'root' })
export class ACMEBankAccountResolve implements Resolve<IACMEBankAccount> {
    constructor(private service: ACMEBankAccountService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((aCMEBankAccount: HttpResponse<ACMEBankAccount>) => aCMEBankAccount.body));
        }
        return of(new ACMEBankAccount());
    }
}

export const aCMEBankAccountRoute: Routes = [
    {
        path: 'acme-bank-account',
        component: ACMEBankAccountComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMEBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-bank-account/:id/view',
        component: ACMEBankAccountDetailComponent,
        resolve: {
            aCMEBankAccount: ACMEBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMEBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-bank-account/new',
        component: ACMEBankAccountUpdateComponent,
        resolve: {
            aCMEBankAccount: ACMEBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMEBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'acme-bank-account/:id/edit',
        component: ACMEBankAccountUpdateComponent,
        resolve: {
            aCMEBankAccount: ACMEBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMEBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const aCMEBankAccountPopupRoute: Routes = [
    {
        path: 'acme-bank-account/:id/delete',
        component: ACMEBankAccountDeletePopupComponent,
        resolve: {
            aCMEBankAccount: ACMEBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ACMEBankAccounts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
