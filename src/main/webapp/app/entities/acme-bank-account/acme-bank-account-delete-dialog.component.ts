import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';
import { ACMEBankAccountService } from './acme-bank-account.service';

@Component({
    selector: 'jhi-acme-bank-account-delete-dialog',
    templateUrl: './acme-bank-account-delete-dialog.component.html'
})
export class ACMEBankAccountDeleteDialogComponent {
    aCMEBankAccount: IACMEBankAccount;

    constructor(
        private aCMEBankAccountService: ACMEBankAccountService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.aCMEBankAccountService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'aCMEBankAccountListModification',
                content: 'Deleted an aCMEBankAccount'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-acme-bank-account-delete-popup',
    template: ''
})
export class ACMEBankAccountDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ aCMEBankAccount }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ACMEBankAccountDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.aCMEBankAccount = aCMEBankAccount;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
