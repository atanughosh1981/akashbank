import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

@Component({
    selector: 'jhi-acme-bank-account-detail',
    templateUrl: './acme-bank-account-detail.component.html'
})
export class ACMEBankAccountDetailComponent implements OnInit {
    aCMEBankAccount: IACMEBankAccount;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ aCMEBankAccount }) => {
            this.aCMEBankAccount = aCMEBankAccount;
        });
    }

    previousState() {
        window.history.back();
    }
}
