package org.jhipster.blog.web.rest;

import org.jhipster.blog.TestApp;

import org.jhipster.blog.domain.ACMETransaction;
import org.jhipster.blog.repository.ACMETransactionRepository;
import org.jhipster.blog.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static org.jhipster.blog.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.jhipster.blog.domain.enumeration.TRANSTYPE;
/**
 * Test class for the ACMETransactionResource REST controller.
 *
 * @see ACMETransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class ACMETransactionResourceIntTest {

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TRANSACTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TRANSACTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_TRANSACTION_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_REMARK = "BBBBBBBBBB";

    private static final TRANSTYPE DEFAULT_TRANSACTION_TYPE = TRANSTYPE.DEBIT;
    private static final TRANSTYPE UPDATED_TRANSACTION_TYPE = TRANSTYPE.CREDIT;

    private static final Double DEFAULT_AVAILABLE_BALANCE = 1D;
    private static final Double UPDATED_AVAILABLE_BALANCE = 2D;

    @Autowired
    private ACMETransactionRepository aCMETransactionRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restACMETransactionMockMvc;

    private ACMETransaction aCMETransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ACMETransactionResource aCMETransactionResource = new ACMETransactionResource(aCMETransactionRepository);
        this.restACMETransactionMockMvc = MockMvcBuilders.standaloneSetup(aCMETransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ACMETransaction createEntity(EntityManager em) {
        ACMETransaction aCMETransaction = new ACMETransaction()
            .accountNumber(DEFAULT_ACCOUNT_NUMBER)
            .transactionDate(DEFAULT_TRANSACTION_DATE)
            .amount(DEFAULT_AMOUNT)
            .transactionRemark(DEFAULT_TRANSACTION_REMARK)
            .transactionType(DEFAULT_TRANSACTION_TYPE)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE);
        return aCMETransaction;
    }

    @Before
    public void initTest() {
        aCMETransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createACMETransaction() throws Exception {
        int databaseSizeBeforeCreate = aCMETransactionRepository.findAll().size();

        // Create the ACMETransaction
        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isCreated());

        // Validate the ACMETransaction in the database
        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeCreate + 1);
        ACMETransaction testACMETransaction = aCMETransactionList.get(aCMETransactionList.size() - 1);
        assertThat(testACMETransaction.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
        assertThat(testACMETransaction.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
        assertThat(testACMETransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testACMETransaction.getTransactionRemark()).isEqualTo(DEFAULT_TRANSACTION_REMARK);
        assertThat(testACMETransaction.getTransactionType()).isEqualTo(DEFAULT_TRANSACTION_TYPE);
        assertThat(testACMETransaction.getAvailableBalance()).isEqualTo(DEFAULT_AVAILABLE_BALANCE);
    }

    @Test
    @Transactional
    public void createACMETransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aCMETransactionRepository.findAll().size();

        // Create the ACMETransaction with an existing ID
        aCMETransaction.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        // Validate the ACMETransaction in the database
        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAccountNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setAccountNumber(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTransactionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setTransactionDate(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setAmount(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTransactionRemarkIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setTransactionRemark(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTransactionTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setTransactionType(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAvailableBalanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMETransactionRepository.findAll().size();
        // set the field null
        aCMETransaction.setAvailableBalance(null);

        // Create the ACMETransaction, which fails.

        restACMETransactionMockMvc.perform(post("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllACMETransactions() throws Exception {
        // Initialize the database
        aCMETransactionRepository.saveAndFlush(aCMETransaction);

        // Get all the aCMETransactionList
        restACMETransactionMockMvc.perform(get("/api/acme-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aCMETransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(DEFAULT_TRANSACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].transactionRemark").value(hasItem(DEFAULT_TRANSACTION_REMARK.toString())))
            .andExpect(jsonPath("$.[*].transactionType").value(hasItem(DEFAULT_TRANSACTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(DEFAULT_AVAILABLE_BALANCE.doubleValue())));
    }
    

    @Test
    @Transactional
    public void getACMETransaction() throws Exception {
        // Initialize the database
        aCMETransactionRepository.saveAndFlush(aCMETransaction);

        // Get the aCMETransaction
        restACMETransactionMockMvc.perform(get("/api/acme-transactions/{id}", aCMETransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aCMETransaction.getId().intValue()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.transactionDate").value(DEFAULT_TRANSACTION_DATE.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.transactionRemark").value(DEFAULT_TRANSACTION_REMARK.toString()))
            .andExpect(jsonPath("$.transactionType").value(DEFAULT_TRANSACTION_TYPE.toString()))
            .andExpect(jsonPath("$.availableBalance").value(DEFAULT_AVAILABLE_BALANCE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingACMETransaction() throws Exception {
        // Get the aCMETransaction
        restACMETransactionMockMvc.perform(get("/api/acme-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateACMETransaction() throws Exception {
        // Initialize the database
        aCMETransactionRepository.saveAndFlush(aCMETransaction);

        int databaseSizeBeforeUpdate = aCMETransactionRepository.findAll().size();

        // Update the aCMETransaction
        ACMETransaction updatedACMETransaction = aCMETransactionRepository.findById(aCMETransaction.getId()).get();
        // Disconnect from session so that the updates on updatedACMETransaction are not directly saved in db
        em.detach(updatedACMETransaction);
        updatedACMETransaction
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .amount(UPDATED_AMOUNT)
            .transactionRemark(UPDATED_TRANSACTION_REMARK)
            .transactionType(UPDATED_TRANSACTION_TYPE)
            .availableBalance(UPDATED_AVAILABLE_BALANCE);

        restACMETransactionMockMvc.perform(put("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedACMETransaction)))
            .andExpect(status().isOk());

        // Validate the ACMETransaction in the database
        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeUpdate);
        ACMETransaction testACMETransaction = aCMETransactionList.get(aCMETransactionList.size() - 1);
        assertThat(testACMETransaction.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
        assertThat(testACMETransaction.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
        assertThat(testACMETransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testACMETransaction.getTransactionRemark()).isEqualTo(UPDATED_TRANSACTION_REMARK);
        assertThat(testACMETransaction.getTransactionType()).isEqualTo(UPDATED_TRANSACTION_TYPE);
        assertThat(testACMETransaction.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingACMETransaction() throws Exception {
        int databaseSizeBeforeUpdate = aCMETransactionRepository.findAll().size();

        // Create the ACMETransaction

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restACMETransactionMockMvc.perform(put("/api/acme-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMETransaction)))
            .andExpect(status().isBadRequest());

        // Validate the ACMETransaction in the database
        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteACMETransaction() throws Exception {
        // Initialize the database
        aCMETransactionRepository.saveAndFlush(aCMETransaction);

        int databaseSizeBeforeDelete = aCMETransactionRepository.findAll().size();

        // Get the aCMETransaction
        restACMETransactionMockMvc.perform(delete("/api/acme-transactions/{id}", aCMETransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ACMETransaction> aCMETransactionList = aCMETransactionRepository.findAll();
        assertThat(aCMETransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ACMETransaction.class);
        ACMETransaction aCMETransaction1 = new ACMETransaction();
        aCMETransaction1.setId(1L);
        ACMETransaction aCMETransaction2 = new ACMETransaction();
        aCMETransaction2.setId(aCMETransaction1.getId());
        assertThat(aCMETransaction1).isEqualTo(aCMETransaction2);
        aCMETransaction2.setId(2L);
        assertThat(aCMETransaction1).isNotEqualTo(aCMETransaction2);
        aCMETransaction1.setId(null);
        assertThat(aCMETransaction1).isNotEqualTo(aCMETransaction2);
    }
}
