package org.jhipster.blog.web.rest;

import org.jhipster.blog.TestApp;

import org.jhipster.blog.domain.ACMEBankAccount;
import org.jhipster.blog.repository.ACMEBankAccountRepository;
import org.jhipster.blog.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static org.jhipster.blog.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ACMEBankAccountResource REST controller.
 *
 * @see ACMEBankAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class ACMEBankAccountResourceIntTest {

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PAN_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PAN_NUMBER = "BBBBBBBBBB";

    @Autowired
    private ACMEBankAccountRepository aCMEBankAccountRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restACMEBankAccountMockMvc;

    private ACMEBankAccount aCMEBankAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ACMEBankAccountResource aCMEBankAccountResource = new ACMEBankAccountResource(aCMEBankAccountRepository);
        this.restACMEBankAccountMockMvc = MockMvcBuilders.standaloneSetup(aCMEBankAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ACMEBankAccount createEntity(EntityManager em) {
        ACMEBankAccount aCMEBankAccount = new ACMEBankAccount()
            .accountNumber(DEFAULT_ACCOUNT_NUMBER)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .address(DEFAULT_ADDRESS)
            .postalCode(DEFAULT_POSTAL_CODE)
            .panNumber(DEFAULT_PAN_NUMBER);
        return aCMEBankAccount;
    }

    @Before
    public void initTest() {
        aCMEBankAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createACMEBankAccount() throws Exception {
        int databaseSizeBeforeCreate = aCMEBankAccountRepository.findAll().size();

        // Create the ACMEBankAccount
        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isCreated());

        // Validate the ACMEBankAccount in the database
        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeCreate + 1);
        ACMEBankAccount testACMEBankAccount = aCMEBankAccountList.get(aCMEBankAccountList.size() - 1);
        assertThat(testACMEBankAccount.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
        assertThat(testACMEBankAccount.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testACMEBankAccount.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testACMEBankAccount.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testACMEBankAccount.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testACMEBankAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testACMEBankAccount.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testACMEBankAccount.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testACMEBankAccount.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testACMEBankAccount.getPanNumber()).isEqualTo(DEFAULT_PAN_NUMBER);
    }

    @Test
    @Transactional
    public void createACMEBankAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aCMEBankAccountRepository.findAll().size();

        // Create the ACMEBankAccount with an existing ID
        aCMEBankAccount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        // Validate the ACMEBankAccount in the database
        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAccountNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMEBankAccountRepository.findAll().size();
        // set the field null
        aCMEBankAccount.setAccountNumber(null);

        // Create the ACMEBankAccount, which fails.

        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMEBankAccountRepository.findAll().size();
        // set the field null
        aCMEBankAccount.setUsername(null);

        // Create the ACMEBankAccount, which fails.

        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMEBankAccountRepository.findAll().size();
        // set the field null
        aCMEBankAccount.setPassword(null);

        // Create the ACMEBankAccount, which fails.

        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMEBankAccountRepository.findAll().size();
        // set the field null
        aCMEBankAccount.setFirstName(null);

        // Create the ACMEBankAccount, which fails.

        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = aCMEBankAccountRepository.findAll().size();
        // set the field null
        aCMEBankAccount.setLastName(null);

        // Create the ACMEBankAccount, which fails.

        restACMEBankAccountMockMvc.perform(post("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllACMEBankAccounts() throws Exception {
        // Initialize the database
        aCMEBankAccountRepository.saveAndFlush(aCMEBankAccount);

        // Get all the aCMEBankAccountList
        restACMEBankAccountMockMvc.perform(get("/api/acme-bank-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aCMEBankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].panNumber").value(hasItem(DEFAULT_PAN_NUMBER.toString())));
    }
    

    @Test
    @Transactional
    public void getACMEBankAccount() throws Exception {
        // Initialize the database
        aCMEBankAccountRepository.saveAndFlush(aCMEBankAccount);

        // Get the aCMEBankAccount
        restACMEBankAccountMockMvc.perform(get("/api/acme-bank-accounts/{id}", aCMEBankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aCMEBankAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.panNumber").value(DEFAULT_PAN_NUMBER.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingACMEBankAccount() throws Exception {
        // Get the aCMEBankAccount
        restACMEBankAccountMockMvc.perform(get("/api/acme-bank-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateACMEBankAccount() throws Exception {
        // Initialize the database
        aCMEBankAccountRepository.saveAndFlush(aCMEBankAccount);

        int databaseSizeBeforeUpdate = aCMEBankAccountRepository.findAll().size();

        // Update the aCMEBankAccount
        ACMEBankAccount updatedACMEBankAccount = aCMEBankAccountRepository.findById(aCMEBankAccount.getId()).get();
        // Disconnect from session so that the updates on updatedACMEBankAccount are not directly saved in db
        em.detach(updatedACMEBankAccount);
        updatedACMEBankAccount
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .postalCode(UPDATED_POSTAL_CODE)
            .panNumber(UPDATED_PAN_NUMBER);

        restACMEBankAccountMockMvc.perform(put("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedACMEBankAccount)))
            .andExpect(status().isOk());

        // Validate the ACMEBankAccount in the database
        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeUpdate);
        ACMEBankAccount testACMEBankAccount = aCMEBankAccountList.get(aCMEBankAccountList.size() - 1);
        assertThat(testACMEBankAccount.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
        assertThat(testACMEBankAccount.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testACMEBankAccount.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testACMEBankAccount.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testACMEBankAccount.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testACMEBankAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testACMEBankAccount.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testACMEBankAccount.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testACMEBankAccount.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testACMEBankAccount.getPanNumber()).isEqualTo(UPDATED_PAN_NUMBER);
    }

    @Test
    @Transactional
    public void updateNonExistingACMEBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = aCMEBankAccountRepository.findAll().size();

        // Create the ACMEBankAccount

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restACMEBankAccountMockMvc.perform(put("/api/acme-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aCMEBankAccount)))
            .andExpect(status().isBadRequest());

        // Validate the ACMEBankAccount in the database
        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteACMEBankAccount() throws Exception {
        // Initialize the database
        aCMEBankAccountRepository.saveAndFlush(aCMEBankAccount);

        int databaseSizeBeforeDelete = aCMEBankAccountRepository.findAll().size();

        // Get the aCMEBankAccount
        restACMEBankAccountMockMvc.perform(delete("/api/acme-bank-accounts/{id}", aCMEBankAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ACMEBankAccount> aCMEBankAccountList = aCMEBankAccountRepository.findAll();
        assertThat(aCMEBankAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ACMEBankAccount.class);
        ACMEBankAccount aCMEBankAccount1 = new ACMEBankAccount();
        aCMEBankAccount1.setId(1L);
        ACMEBankAccount aCMEBankAccount2 = new ACMEBankAccount();
        aCMEBankAccount2.setId(aCMEBankAccount1.getId());
        assertThat(aCMEBankAccount1).isEqualTo(aCMEBankAccount2);
        aCMEBankAccount2.setId(2L);
        assertThat(aCMEBankAccount1).isNotEqualTo(aCMEBankAccount2);
        aCMEBankAccount1.setId(null);
        assertThat(aCMEBankAccount1).isNotEqualTo(aCMEBankAccount2);
    }
}
