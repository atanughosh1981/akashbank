/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TestTestModule } from '../../../test.module';
import { ACMEBankAccountDetailComponent } from 'app/entities/acme-bank-account/acme-bank-account-detail.component';
import { ACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

describe('Component Tests', () => {
    describe('ACMEBankAccount Management Detail Component', () => {
        let comp: ACMEBankAccountDetailComponent;
        let fixture: ComponentFixture<ACMEBankAccountDetailComponent>;
        const route = ({ data: of({ aCMEBankAccount: new ACMEBankAccount(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [ACMEBankAccountDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ACMEBankAccountDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ACMEBankAccountDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.aCMEBankAccount).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
