/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { ACMEBankAccountComponent } from 'app/entities/acme-bank-account/acme-bank-account.component';
import { ACMEBankAccountService } from 'app/entities/acme-bank-account/acme-bank-account.service';
import { ACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

describe('Component Tests', () => {
    describe('ACMEBankAccount Management Component', () => {
        let comp: ACMEBankAccountComponent;
        let fixture: ComponentFixture<ACMEBankAccountComponent>;
        let service: ACMEBankAccountService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [ACMEBankAccountComponent],
                providers: []
            })
                .overrideTemplate(ACMEBankAccountComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ACMEBankAccountComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMEBankAccountService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ACMEBankAccount(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.aCMEBankAccounts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
