/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TestTestModule } from '../../../test.module';
import { ACMEBankAccountDeleteDialogComponent } from 'app/entities/acme-bank-account/acme-bank-account-delete-dialog.component';
import { ACMEBankAccountService } from 'app/entities/acme-bank-account/acme-bank-account.service';

describe('Component Tests', () => {
    describe('ACMEBankAccount Management Delete Component', () => {
        let comp: ACMEBankAccountDeleteDialogComponent;
        let fixture: ComponentFixture<ACMEBankAccountDeleteDialogComponent>;
        let service: ACMEBankAccountService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [ACMEBankAccountDeleteDialogComponent]
            })
                .overrideTemplate(ACMEBankAccountDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ACMEBankAccountDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMEBankAccountService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
