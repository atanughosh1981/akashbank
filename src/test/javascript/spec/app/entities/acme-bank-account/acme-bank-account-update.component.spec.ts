/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TestTestModule } from '../../../test.module';
import { ACMEBankAccountUpdateComponent } from 'app/entities/acme-bank-account/acme-bank-account-update.component';
import { ACMEBankAccountService } from 'app/entities/acme-bank-account/acme-bank-account.service';
import { ACMEBankAccount } from 'app/shared/model/acme-bank-account.model';

describe('Component Tests', () => {
    describe('ACMEBankAccount Management Update Component', () => {
        let comp: ACMEBankAccountUpdateComponent;
        let fixture: ComponentFixture<ACMEBankAccountUpdateComponent>;
        let service: ACMEBankAccountService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [ACMEBankAccountUpdateComponent]
            })
                .overrideTemplate(ACMEBankAccountUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ACMEBankAccountUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMEBankAccountService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ACMEBankAccount(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.aCMEBankAccount = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ACMEBankAccount();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.aCMEBankAccount = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
