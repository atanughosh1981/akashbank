/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { ACMETransactionComponent } from 'app/entities/acme-transaction/acme-transaction.component';
import { ACMETransactionService } from 'app/entities/acme-transaction/acme-transaction.service';
import { ACMETransaction } from 'app/shared/model/acme-transaction.model';

describe('Component Tests', () => {
    describe('ACMETransaction Management Component', () => {
        let comp: ACMETransactionComponent;
        let fixture: ComponentFixture<ACMETransactionComponent>;
        let service: ACMETransactionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [ACMETransactionComponent],
                providers: []
            })
                .overrideTemplate(ACMETransactionComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ACMETransactionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ACMETransactionService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ACMETransaction(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.aCMETransactions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
